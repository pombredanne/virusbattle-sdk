# Creating frozen package

## Install pyinstaller

https://pyinstaller.readthedocs.io/en/stable/installation.html

    pip install pyinstaller

If on Windows, install python 2.7.12 or greater. Pip will be installed by 
default.

## Freeze package

    pyinstaller -F ./vbclient.py

Frozen executable can be found at ./dist/vbclient

You must freeze on the OS you want to ship to. There is no cross-compiling with 
pyinstaller.

