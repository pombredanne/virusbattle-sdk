================================
Cythereal SDK Usage Instructions
================================

The VirusBattle SDK (vbsdk) is a library to access Cythereal's
Malware Genomic Correlation (MAGIC) Search and Cythereal's VirusBattle
Automated Malware Analysis webservice.

Installing this package also creates the ``vbclient`` command that can be used
from the command line.

You can download just the `Linux client`_ or the `Windows client`_ if you don't
need access to the full sdk library.

.. _Linux client: https://bitbucket.org/cythereal/virusbattle-sdk/downloads/vbclient
.. _Windows client: https://bitbucket.org/cythereal/virusbattle-sdk/downloads/vbclient.exe

* **Source**: https://bitbucket.org/cythereal/virusbattle-sdk/src
* **More Info**: https://bitbucket.org/cythereal/virusbattle-sdk/wiki/Home
